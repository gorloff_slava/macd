<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use GuzzleHttp\Client;

class AddRealData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('countries', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();

            $table->string('name');
            $table->string('capital');
            $table->string('continent');
            $table->double('area');
            $table->integer('population');
            $table->text('comments');

            //Нужно для рабочей сортировки
            $table->string('fix_category')->default('fix');
        });

        if (env('START_FROM_INTERNET', false) == true) {
            $client = new Client();
            $respone = $client->get('http://api.geonames.org/countryInfo?username=Slavcha&lang=ru');
            $data = $respone->getBody()->getContents();
        } else {
            $data = file_get_contents(database_path('migrations/countryInfo.xml'));
        }

        $xml   = simplexml_load_string($data);
        $array = json_decode(json_encode((array) $xml), true);
//        $array = array($xml->getName() => $array);
        $countries = collect($array);
        foreach ($countries->first() as $tempCountry) {
            $country = (object)$tempCountry;
            if ($country->capital === []) {
                $country->capital = 'null';
            }
            $resultCountry = \App\Country::create([
                'name' => $country->countryName,
                'capital' => $country->capital,
                'continent' => $country->continentName,
                'area' => $country->areaInSqKm,
                'population' => $country->population,
                'comments' => json_encode($country->languages)
            ]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
